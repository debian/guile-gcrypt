Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: guile-gcrypt
Source: https://notabug.org/cwebber/guile-gcrypt

Files: *
Copyright: 2012-2022 Ludovic Courtès <ludo@gnu.org>
 2014 Nikita Karetnikov <nikita@karetnikov.org>
 2015-2017 Christopher Allan Webber <cwebber@dustycloud.org>
 2013-2014 David Thompson <dthompson2@worcester.edu>
 2015 David Thompson <davet@gnu.org>
 2015-2016 Mathieu Lirzin <mthl@gnu.org>
 2014 Mark H Weaver <mhw@netris.org>
 2019 Mathieu Othacehe <m.othacehe@gmail.com>
License: GPL-3.0+

Files: debian/*
Copyright: 2019 Vagrant Cascadian <vagrant@debian.org>
License: GPL-3.0+

Files: gcrypt/base64.scm
Copyright: 2009-2018 Göran Weinholt <goran@weinholt.se>
License: Expat and GPL-3.0+

Files: guile-gcrypt.texi
Copyright: 2018-2022 Ludovic Courtès
License: GFDL-1.3+

Files: NEWS
Copyright: 2019-2022 Ludovic Courtès <ludo@gnu.org>
License: Permissive

License: GPL-3.0+
 This program is free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public License
 Version 3 can be found in `/usr/share/common-licenses/GPL-3'.

License: GFDL-1.3+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3 or
 any later version published by the Free Software Foundation; with no
 Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
 copy of the license is included in the section entitled ``GNU Free
 Documentation License''.
 .
 On Debian systems, the complete text of the GNU Free Documentation License
 Version 1.3 can be found in `/usr/share/common-licenses/GFDL-1.3'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: Permissive
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved.
